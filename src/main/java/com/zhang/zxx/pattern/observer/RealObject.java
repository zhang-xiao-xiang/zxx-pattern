package com.zhang.zxx.pattern.observer;

/**
 * RealObject:真实订阅者
 *
 * @author zhangxiaoxiang
 * @date 6/7/2021
 */
public class RealObject implements Observer {
    /**
     * 订阅者名称
     */
    private String name;

    public RealObject(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        Subject subject = new RealSubject();
        Observer observer = new RealObject("张晓祥");
        Observer observer2 = new RealObject("长草颜团子");
        Observer observer3 = new RealObject("小乔");
        subject.attach(observer);
        subject.attach(observer2);
        subject.attach(observer3);
        subject.notifyChanged("你好,今天主题是:设计模式之观察者模式");
    }

    @Override
    public void update(String msg) {
        System.out.println(name + msg);
    }

}
