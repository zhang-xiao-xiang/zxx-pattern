package com.zhang.zxx.pattern.observer;

import java.util.ArrayList;
import java.util.List;

public class RealSubject implements Subject {
    String name;


    private List<Observer> observerList = new ArrayList<>();

    @Override
    public void attach(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyChanged(String msg) {
        for (Observer observer : observerList) {
            observer.update(msg);
        }
    }
}