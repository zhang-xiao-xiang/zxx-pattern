package com.zhang.zxx.pattern.observer;


/**
 * Subject:观察者主题对象
 *
 * @author zhangxiaoxiang
 * @date 6/7/2021
 */
public interface Subject {

    /**
     * 订阅操作
     *
     * @param observer observer
     */
    void attach(Observer observer);

    /**
     * 取消订阅操作
     *
     * @param observer observer
     */
    void detach(Observer observer);

    /**
     * 变动通知
     *
     * @param msg 通知消息
     */
    void notifyChanged(String msg);
}
