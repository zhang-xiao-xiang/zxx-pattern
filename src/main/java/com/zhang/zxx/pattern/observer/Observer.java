package com.zhang.zxx.pattern.observer;


/**
 * Observer:观察者订阅人对象
 *
 * @author zhangxiaoxiang
 * @date 6/7/2021
 */
public interface Observer {


    /**
     * 接收变动通知
     *
     * @param msg 通知消息
     */
    void update(String msg);
}
