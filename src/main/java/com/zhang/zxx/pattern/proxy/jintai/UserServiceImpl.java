package com.zhang.zxx.pattern.proxy.jintai;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * UserServiceImpl:女朋友亲自逛街的实现
 *
 * @author zhangxiaoxiang
 * @date 2021/07/03
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Override
    public String shopping(String place) {
        log.info("女朋友在 [{}] 逛街购物", place);
        return "LV包包";
    }

    /**
     * 了解
     * 这个是一个普通方法，没有在接口里面，静态代理无法实现，但是用装饰器模式可以去做，这个就是和装饰器模式的区别
     *
     * @param place 逛街地点(就是模拟一个参数,意思意思)
     * @return 逛街收货
     */
    public String shoppingV2(String place) {
        log.info("本小姐在 [{}] 逛街购物", place);
        return "LV包包";
    }
}
