package com.zhang.zxx.pattern.proxy.jdkdongtai;

/**
 * UserService:用户接口
 *
 * @author zhangxiaoxiang
 * @date 2021/07/03
 */
public interface UserV2Service {
    /**
     * 逛街
     *
     * @param place 逛街地点(就是模拟一个参数,意思意思)
     * @return 逛街收货
     */
    String shopping(String place);
}
