package com.zhang.zxx.pattern.proxy.cglib;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * UserServiceV3:这个是个类,没有实现接口
 *
 * @author zhangxiaoxiang
 * @date 2021/07/06
 */
@Slf4j
@Service
public class UserV3Service {
    /**
     * 逛街
     *
     * @param place 逛街地点(就是模拟一个参数,意思意思)
     * @return 逛街收货
     */
    String shopping(String place) {
        log.info("女朋友在 [{}] 逛街购物", place);
        return "LV包包";
    }
}
