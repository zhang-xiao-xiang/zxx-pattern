package com.zhang.zxx.pattern.proxy.jintai;

/**
 * UserService:静态代理的接口(此接口里面的方法就是需要代理的目标)
 *
 * @author zhangxiaoxiang
 * @date 2021/07/03
 */
public interface UserService {
    /**
     * 逛街
     *
     * @param place 逛街地点(就是模拟一个参数,意思意思)
     * @return 逛街收货
     */
    String shopping(String place);
}
