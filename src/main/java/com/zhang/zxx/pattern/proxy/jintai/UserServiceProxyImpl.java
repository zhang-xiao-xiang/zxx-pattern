package com.zhang.zxx.pattern.proxy.jintai;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * UserServiceProxyImpl:代理对象去代理实现对应的接口方法
 *
 * @author zhangxiaoxiang
 * @date 2021/07/03
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceProxyImpl implements UserService {

    /**
     * 你男朋友能干的,其他男朋友也必须得会[这里就是面向接口编程思想的提现]
     */
    private final UserService target;

    @Override
    public String shopping(String place) {
        log.info("[静态代理]开着法拉利到小区接你");
        String shopping = target.shopping(place);
        log.info("[静态代理]开着法拉利送你回家");
        return shopping;
    }
    //    优点是在不改变原来的实现类的情况下对方法实现了增强
    //    缺点是如果原来的接口新增方法[比如新增旅游],这里也要跟着改,牵一发,动全身
}
