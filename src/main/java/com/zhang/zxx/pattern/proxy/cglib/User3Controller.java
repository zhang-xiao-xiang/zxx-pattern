package com.zhang.zxx.pattern.proxy.cglib;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * User3Controller:
 *
 * @author zhangxiaoxiang
 * @date 2021/07/06
 */
@RestController
@RequestMapping("/cglib")
@RequiredArgsConstructor
public class User3Controller {
    private final UserV3Service userV3Service;

    @PostMapping("/shopping")
    public String shopping() {
        UserV3Service proxyInstance = (UserV3Service) new CglibProxy(userV3Service).getProxyInstance();
        return proxyInstance.shopping("解放碑");
    }
}
