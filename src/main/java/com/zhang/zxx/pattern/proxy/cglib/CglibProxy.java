package com.zhang.zxx.pattern.proxy.cglib;


import lombok.extern.slf4j.Slf4j;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * JDKProxy:Cglib子类代理工厂[注意包名]
 * 1.需要引入cglib的jar文件,但是Spring的核心包中已经包括了Cglib功能,所以直接引入pring-core-3.2.5.jar即可.
 * 2.引入功能包后,就可以在内存中动态构建子类
 * 3.代理的类不能为final,否则报错
 * 4.目标对象的方法如果为final/static,那么就不会被拦截,即不会执行目标对象额外的业务方法.
 *
 * @author zhangxiaoxiang
 * @date 2021/07/06
 */
@Slf4j
public class CglibProxy implements MethodInterceptor {
    /**
     * 维护目标对象
     */
    private final Object target;

    public CglibProxy(Object target) {
        this.target = target;
    }

    /**
     * 给目标对象创建一个代理对象
     *
     * @return Object
     */
    public Object getProxyInstance() {
        // 1.工具类
        Enhancer en = new Enhancer();
        // 2.设置父类
        en.setSuperclass(target.getClass());
        // 3.设置回调函数
        en.setCallback(this);
        // 4.创建子类(代理对象)
        return en.create();

    }

    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        log.info("[cglib动态代理]开着法拉利到小区接你");
        // 执行目标对象的方法
        Object returnValue = method.invoke(target, args);
        log.info("[cglib动态代理]开着法拉利送你回家");
        return returnValue;
    }
}
