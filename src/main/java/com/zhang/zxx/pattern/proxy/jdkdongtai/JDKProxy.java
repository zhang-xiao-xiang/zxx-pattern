package com.zhang.zxx.pattern.proxy.jdkdongtai;


import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Proxy;

/**
 * JDKProxy:把代理对象抽象一下,整一个ProxyFactory,俗称海王
 *
 * @author zhangxiaoxiang
 * @date 2021/07/03
 */
@Slf4j
public class JDKProxy {
    /**
     * 维护一个目标对象
     */
    private Object target;

    public JDKProxy(Object target) {
        this.target = target;
    }

    /**
     * 给目标对象生成代理对象
     *
     * @return jdk代理生成的对象
     */
    public Object getProxyInstance() {
        return Proxy.newProxyInstance(
                target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                // 这里其实是要实现jdk代理InvocationHandler的接口,然后改成JKD8的写法了而已
                (proxy, method, args) -> {
                    log.info("[JDK动态代理]开着法拉利到小区接你");
                    // 执行目标对象方法
                    Object returnValue = method.invoke(target, args);
                    log.info("[JDK动态代理]开着法拉利送你回家");
                    return returnValue;
                }
        );
    }
}
