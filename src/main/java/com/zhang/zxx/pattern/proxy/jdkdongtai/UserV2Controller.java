package com.zhang.zxx.pattern.proxy.jdkdongtai;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * UserController:用户控制层(这里当做测试类)---接口名称尽量按照REST 风格来的
 *
 * @author zhangxiaoxiang
 * @date 2021/07/03
 */
@RestController
@RequestMapping("/jdkproxy")
@RequiredArgsConstructor
public class UserV2Controller {
    private final UserV2ServiceImpl userServiceV2;

    @PostMapping("/shopping")
    public String shopping() {
        UserV2Service proxyInstance = (UserV2Service) new JDKProxy(userServiceV2).getProxyInstance();
        return proxyInstance.shopping("解放碑");
    }
}
