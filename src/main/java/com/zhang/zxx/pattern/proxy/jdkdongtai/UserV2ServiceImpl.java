package com.zhang.zxx.pattern.proxy.jdkdongtai;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * UserServiceImpl:实现类
 *
 * @author zhangxiaoxiang
 * @date 2021/07/03
 */
@Slf4j
@Service
public class UserV2ServiceImpl implements UserV2Service {
    @Override
    public String shopping(String place) {
        log.info("女朋友在 [{}] 逛街购物", place);
        return "LV包包";
    }
}
