package com.zhang.zxx.pattern.proxy.jintai;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * UserController:用户控制层(这里当做测试类)---接口名称尽量按照REST 风格来的
 *
 * @author zhangxiaoxiang
 * @date 2021/07/03
 */
@RestController
@RequestMapping("/proxy")
@RequiredArgsConstructor
public class UserController {
    // 顺便看一下一个接口2个实现类的基本注入: https://www.cnblogs.com/leeego-123/p/10882069.html

    private final UserServiceImpl userService;

    private final UserServiceProxyImpl userServiceProxyImpl;

    @PostMapping("/shopping")
    public String saveUser() {
        return userService.shopping("解放碑");
    }

    @PostMapping("/shoppingproxy")
    public String shopping() {
        return userServiceProxyImpl.shopping("解放碑");
    }
}
