package com.zhang.zxx.pattern.strategy.strategy;

import com.zhang.zxx.pattern.strategy.LoginEnum;
import com.zhang.zxx.pattern.strategy.StrategyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * DefaultStrategyImpl:默认策略(相当于没有匹配请求参数返回的默认策略),一般策略模式都会带上默认策略,类似if else最后都有个else 或者switch语句的默认分支
 *
 * @author zhangxiaoxiang
 * @date 2021/07/18
 */
@Service
@Slf4j
public class DefaultStrategyImpl implements StrategyService {

    @Override
    public Integer matchKey() {
        return LoginEnum.DEFAULT.getKey();
    }

    @Override
    public Object handler(Integer key) {
        log.info("用户根据[默认登录策略],执行相关逻辑");
        return "[默认登录策略]";
    }
}
