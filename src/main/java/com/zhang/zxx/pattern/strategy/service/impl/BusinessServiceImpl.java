package com.zhang.zxx.pattern.strategy.service.impl;

import com.zhang.zxx.pattern.strategy.LoginEnum;
import com.zhang.zxx.pattern.strategy.StrategyHandler;
import com.zhang.zxx.pattern.strategy.service.BusinessService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * BusinessServiceImpl:模拟业务遇到的实现类(控制层controller->service层->dao层)
 *
 * @author zhangxiaoxiang
 * @date 2021/07/18
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class BusinessServiceImpl implements BusinessService {
    private final StrategyHandler strategyhandler;

    @Override
    public Object doLoginWithOutStrategy(Integer key) {
        if (Objects.equals(LoginEnum.ACCOUNT.getKey(), key)) {
            log.info("用户根据[账号密码登录],执行相关逻辑===>未使用策略模式");
            return "[账号密码登录]";
        } else if (Objects.equals(LoginEnum.QQ.getKey(), key)) {
            log.info("用户根据[QQ登录],执行相关逻辑===>未使用策略模式");
            return "[QQ登录]";
        } else if (Objects.equals(LoginEnum.WECHAT.getKey(), key)) {
            log.info("用户根据[微信登录],执行相关逻辑===>未使用策略模式");
            return "[微信登录]";
        } else {
            log.info("用户根据[默认登录策略],执行相关逻辑===>未使用策略模式");
            return "[默认登录策略]";
        }
    }

    @Override
    public Object doLoginWithStrategy(Integer key) {
        // 满足开闭原则,这里不管新增登录类型key还是修改类型key内部逻辑,这里都不需动,也不需要动它,闭就是禁止在这里修改,维护就很棒了
        return strategyhandler.handler(key);
    }
}
