package com.zhang.zxx.pattern.strategy;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * MyEnum:登录类型枚举[其它业务 比如支付类型,物流类型,运营商类型类比即可],高中学的分类讨论思想
 *
 * @author zhangxiaoxiang
 * @date 2021/07/17
 */
@AllArgsConstructor
public enum LoginEnum {
    /**
     * 账号
     */
    ACCOUNT(1, "账号登录"),
    /**
     * QQ登录
     */
    QQ(2, "QQ登录"),

    /**
     * 微信登录
     */
    WECHAT(3, "微信登录"),
    /**
     * 暂不支持登录方式
     */
    DEFAULT(0, "暂不支持登录方式");

    /**
     * 标识key
     */
    @Getter
    @Setter
    private Integer key;
    /**
     * 标识value值
     */
    @Getter
    @Setter
    private String value;
}
