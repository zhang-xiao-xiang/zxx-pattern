package com.zhang.zxx.pattern.strategy.strategy;

import com.zhang.zxx.pattern.strategy.LoginEnum;
import com.zhang.zxx.pattern.strategy.StrategyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * QQLoginStrategyImpl:QQ登录
 *
 * @author zhangxiaoxiang
 * @date 2021/07/18
 */
@Slf4j
@Service
public class QQLoginStrategyImpl implements StrategyService {

    @Override
    public Integer matchKey() {
        return LoginEnum.QQ.getKey();
    }

    @Override
    public Object handler(Integer key) {
        log.info("用户根据[QQ登录],执行相关逻辑");
        return "[QQ登录]";
    }
}
