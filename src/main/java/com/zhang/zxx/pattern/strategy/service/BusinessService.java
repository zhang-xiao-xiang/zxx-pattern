package com.zhang.zxx.pattern.strategy.service;

/**
 * BusinessService:业务服务层
 *
 * @author zhangxiaoxiang
 * @date 2021/07/18
 */
public interface BusinessService {
    /**
     * 未使用策略模式的登录
     *
     * @param key 登录类型
     * @return 登录结果
     */
    Object doLoginWithOutStrategy(Integer key);

    /**
     * 使用策略模式登录[请求参数和返回参数自己根据具体业务设计就行,这里只是就请求参数传个枚举key,返回参数返回个Object为例子哈]
     *
     * @param key 枚举的key
     * @return 返回值[可以为void, 根据你的业务设计就行]
     */
    Object doLoginWithStrategy(Integer key);
}
