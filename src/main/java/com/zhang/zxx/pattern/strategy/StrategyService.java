package com.zhang.zxx.pattern.strategy;

/**
 * 登录策略
 *
 * @author zhangxiaoxiang
 * @date 17/7/2021
 */
public interface StrategyService {
    /**
     * 需要返回枚举的key,用于处理注册对应逻辑
     *
     * @return 枚举的key
     */
    Integer matchKey();


    /**
     * 执行具体策略,这里写业务代码[拓展一种登录模式,只需要新增一个一个枚举值和对应的实现类就行]
     *
     * @param key 具有逻辑请求需要的参数
     * @return 具体逻辑执行完毕返回的参数
     */
    Object handler(Integer key);
}