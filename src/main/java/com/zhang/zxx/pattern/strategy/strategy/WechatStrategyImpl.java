package com.zhang.zxx.pattern.strategy.strategy;

import com.zhang.zxx.pattern.strategy.LoginEnum;
import com.zhang.zxx.pattern.strategy.StrategyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * WechatStrategyImpl:微信登录逻辑
 *
 * @author zhangxiaoxiang
 * @date 2021/07/18
 */
@Slf4j
@Service
public class WechatStrategyImpl implements StrategyService {

    @Override
    public Integer matchKey() {
        return LoginEnum.WECHAT.getKey();
    }

    @Override
    public Object handler(Integer key) {
        log.info("用户根据[微信登录],执行相关逻辑");
        return "[微信登录]";
    }
}
