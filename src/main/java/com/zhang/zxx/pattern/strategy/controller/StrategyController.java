package com.zhang.zxx.pattern.strategy.controller;

import com.zhang.zxx.pattern.strategy.service.BusinessService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * StrategyController:控制层
 *
 * @author zhangxiaoxiang
 * @date 2021/07/18
 */
@Slf4j
@RestController
@RequestMapping("/strategy")
@RequiredArgsConstructor
public class StrategyController {
    private final BusinessService businessService;

    @GetMapping("/{key}")
    public Object doLoginWithOutStrategy(@PathVariable("key") Integer key) {
        // 这里顺带使用REST 风格接口 没有处理异常的和校验这些,因为这里重点是展示策略模式
        log.info("未使用策略模式控制层");
        return businessService.doLoginWithOutStrategy(key);
    }

    @GetMapping("/v2/{key}")
    public Object doLoginWithStrategy(@PathVariable("key") Integer key) {
        log.info("******使用策略模式控制层******");
        return businessService.doLoginWithStrategy(key);
    }
}
