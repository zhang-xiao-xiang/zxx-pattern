package com.zhang.zxx.pattern;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ZxxPatternApplication:设计模式示例项目
 *
 * @author zhangxiaoxiang
 * @date 3/7/2021
 */
@SpringBootApplication
@Slf4j
public class ZxxPatternApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZxxPatternApplication.class, args);
        log.info("设计模式项目示例已启动");
    }

}
